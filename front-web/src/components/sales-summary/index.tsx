import './styles.css';
import SalesSummartyCard from './sales-summarty-card';
import { ReactComponent as AvatarIcon } from '../../assets/avatar-icon.svg';
import { ReactComponent as BarChartIcon } from '../../assets/bar-chart-icon.svg';
import { ReactComponent as DoneIcon } from '../../assets/done-icon.svg';
import { ReactComponent as SyncIcon } from '../../assets/sync-icon.svg';
import { FilterData, SalesSummaryData } from '../../types';
import { useEffect, useMemo, useState } from 'react';
import { buildFilterParams, makeRequest } from '../../utils/request';

type Props = {
   filterData?: FilterData;
};

const initialSummary = {
   avg: 0,
   count: 0,
   max: 0,
   min: 0
};

function SalesSummary({ filterData }: Props) {
   const [summary, setSummary] = useState<SalesSummaryData>(initialSummary);
   const params = useMemo(() => buildFilterParams(filterData), [filterData]);

   useEffect(() => {
      makeRequest
         .get<SalesSummaryData>('/sales/summary', { params })
         .then((response) => {
            setSummary(response.data);
         })
         .catch(() => {
            console.error('Error fetch summary');
         });
   }, [params]);

   return (
      <div className="sales-sumary-container">
         <SalesSummartyCard value={summary?.avg?.toFixed(2)} label="Média" icon={<DoneIcon />} />
         <SalesSummartyCard value={summary?.count} label="Quantidade" icon={<SyncIcon />} />
         <SalesSummartyCard value={summary?.min} label="Mínima" icon={<BarChartIcon />} />
         <SalesSummartyCard value={summary?.max} label="Máxima" icon={<AvatarIcon />} />
      </div>
   );
}

export default SalesSummary;
